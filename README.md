修改自：[ucas-srun](https://github.com/zcq15/ucas-srun)，修改了URLs以在新科院校园网使用，测试时间2024年3月23日。


# 文件说明
下线脚本似乎会踢掉所有在线设备，不管了将就着用。

|文件|说明|
|:-:|:-:|
|BitSrunLogin/|深澜登录函数包|
|login.py|登录示例脚本|
|logout.py|下线示例脚本|
|always_online.py|在线监测脚本，如果监测到掉线则自动重连|

always_online.py可采用`nohup`命令挂在后台：
``` bash
nohup python always_online.py &
```
